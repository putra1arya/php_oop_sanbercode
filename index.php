<?php
    require_once 'animal.php';
    require_once 'frog.php';
    require_once 'ape.php';

    $sheep = new Animal("shaun");

    echo 'Nama Hewan : '.$sheep->name . '<br>'; // "shaun"
    echo 'Jumlah Kaki : '.$sheep->legs . '<br>'; // 2
    echo 'Tipe Darah : '.$sheep->cold_blooded . '<br>'; // false
    
    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

    echo '<br>';
    $sungokong = new Ape("kera sakti");
    $sungokong->yell();// "Auooo"

    echo '<br>';
    $kodok = new Frog("buduk");
    $kodok->get_all();
    $kodok->jump() ; // "hop hop"    
?>