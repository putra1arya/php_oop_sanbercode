<?php
    class frog extends animal {
        public $legs = 4;

        public function __construct($name)
        {
            $this->name = $name;
            echo 'Nama hewan :'.$this->name;
        }

        public function set_blood(){
            $this->cold_blooded = 'true';
        }
        public function get_all(){
            echo '<br> Jumlah Kaki : '. $this->legs . '<br>';
            $this->set_blood();
            echo 'Tipe Darah : '. $this->cold_blooded .'<br>';
        }


        public function jump(){
            echo ' Suaranya : hop hop <br>';
        }
    }
?>